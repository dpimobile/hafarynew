package com.digitalprizm.hafary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Belal on 1/27/2017.
 */

public class NetworkStateChecker extends BroadcastReceiver {

    //context and database helper object
    private Context context;
    private GPSDatabase db;
    String DBName, ContactTableSchema, TABLENAME,DeviceID,mode;
    SQLiteDatabase dbs;
    String lat,name,st1;
    String lon;
    long Id,imgId;
    String REGISTER_URL,img1,img2,img3,img4;
    String encodimg=null;
    Context mcontext;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
//
        db = new GPSDatabase(context);

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        //if there is a network
        if (activeNetwork != null) {
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {

                try {
                    db.open();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Cursor cursor = db.getAllRows();
                if (cursor.moveToFirst()) {
                    do {

                        //calling the method to save the unsynced name to MySQL
//
                        Id = cursor.getLong(cursor.getColumnIndex(db.COLUMN1));
                        lat = cursor.getString(cursor.getColumnIndex(db.COLUMN2));
                        lon=cursor.getString(cursor.getColumnIndex(db.COLUMN3));
                        name=cursor.getString(cursor.getColumnIndex(db.COLUMN4));
                        st1=cursor.getString(cursor.getColumnIndex(db.COLUMN5));
                        if(st1.equals("0")) {
                            saveName();
                            db.deleteRow(Id);
                        }
                        if(st1.equals("1")) {
                            saveStart();
                            db.deleteRow(Id);
                        }
                        if(st1.equals("2")) {
                            saveStop();
                            db.deleteRow(Id);
                        }
                    } while (cursor.moveToNext());
                }
                Cursor cr = db.getAllimgRows();
                if (cr.moveToFirst()) {
                    do {

                        //calling the method to save the unsynced name to MySQL

                        imgId = cr.getLong(cr.getColumnIndex(db.COLUMN6));
                        name=cr.getString(cr.getColumnIndex(db.COLUMN11));
                        byte[] image = cr.getBlob(cr.getColumnIndex(db.COLUMN7));
                       img1= Base64.encodeToString(image, Base64.DEFAULT);

                       // img1 = cr.getString(cr.getColumnIndex(db.COLUMN7));
                        img2=cr.getString(cr.getColumnIndex(db.COLUMN8));
                        img3=cr.getString(cr.getColumnIndex(db.COLUMN9));
                        img4=cr.getString(cr.getColumnIndex(db.COLUMN10));

                            UploadImage();



                    } while (cr.moveToNext());
                }
            }
        }
    }

    /*
    * method taking two arguments
    * name that is to be saved and id of the name from SQLite
    * if the name is successfully sent
    * we will update the status as synced in SQLite
    * */
    private void saveName() {
        SharedPreferences myPrefs = context.getSharedPreferences(Driver_Det.MyPREFERENCES, MODE_PRIVATE);
        String usr = myPrefs.getString("vehicalno.", "DEFAULT VALUE");
        String encodlat= null;
        String encodlon=null;
        String encod=null;

        final GPSDatabase myDatabase=new GPSDatabase(context);
        try {
            encodlat = URLEncoder.encode(lat,"UTF-8");
            encodlon = URLEncoder.encode(lon,"UTF-8");
            encod=URLEncoder.encode(name,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_driving_in_ds?name=" + encod +"&lat=" + encodlat + "&lon=" + encodlon;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                                //updating the status in sqlite
                        //        db.updateNameStatus(id, MainActivity.NAME_SYNCED_WITH_SERVER);

                                //sending the broadcast to refresh the list
                                Toast.makeText(context, "Offline path location updated to server", Toast.LENGTH_SHORT).show();
                               //myDatabase.deleteRow(Id);
                               // context.sendBroadcast(new Intent(DeliveryList.DATA_SAVED_BROADCAST));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(context, "Error in saving offline data", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    private void saveStart() {

        String encodlat= null;
        String encodlon=null;
        String encod=null;

        final GPSDatabase myDatabase=new GPSDatabase(context);
        try {
            encodlat = URLEncoder.encode(lat,"UTF-8");
            encodlon = URLEncoder.encode(lon,"UTF-8");
            encod=URLEncoder.encode(name,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_start_loc_in_ds?name=" + encod + "&lat=" + encodlat + "&lon=" + encodlon;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            //updating the status in sqlite
                            //        db.updateNameStatus(id, MainActivity.NAME_SYNCED_WITH_SERVER);

                            //sending the broadcast to refresh the list
                            Toast.makeText(context, "Offline start location updated to server", Toast.LENGTH_SHORT).show();
                            // myDatabase.deleteRow(Id);
                            // context.sendBroadcast(new Intent(DeliveryList.DATA_SAVED_BROADCAST));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(context, "Error in saving offline data", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    private void saveStop() {

        String encodlat= null;
        String encodlon=null;
        String encod=null;

        final GPSDatabase myDatabase=new GPSDatabase(context);
        try {
            encodlat = URLEncoder.encode(lat,"UTF-8");
            encodlon = URLEncoder.encode(lon,"UTF-8");
            encod=URLEncoder.encode(name,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_stop_loc_in_ds?name=" + encod + "&lat=" + encodlat + "&lon=" + encodlon;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            //updating the status in sqlite
                            //        db.updateNameStatus(id, MainActivity.NAME_SYNCED_WITH_SERVER);

                            //sending the broadcast to refresh the list
                            Toast.makeText(context, "Offline stop location updated to server", Toast.LENGTH_SHORT).show();
                            // myDatabase.deleteRow(Id);
                            // context.sendBroadcast(new Intent(DeliveryList.DATA_SAVED_BROADCAST));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(context, "Error in saving offline data", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
    private void UploadImage() {


        try{
            //encodeimg = URLEncoder.encode(imageString,"UTF-8");
            encodimg=URLEncoder.encode(name,"UTF-8");
        }catch(Exception e){};
        REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_img_in_delivery_schedule";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context,"offline image updated to server",Toast.LENGTH_LONG).show();
                        db.deleteimgRow(Id);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_LONG).show();
                        Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();

                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name",encodimg );
                if (img2== null && img3== null && img4 == null) {
                    params.put("img_1", img1);

                }else if(img3 == null && img4 == null) {

                    params.put("img_2", img2);
                }else if(img4== null) {


                    params.put("img_3", img3);
                }else {
                    params.put("img_1", img1);
                    params.put("img_2", img2);
                    params.put("img_3", img3);
                    params.put("img_4", img4);
               }
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                70000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}
