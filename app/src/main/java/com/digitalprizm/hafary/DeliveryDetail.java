package com.digitalprizm.hafary;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_LONG;

public class DeliveryDetail extends BaseActivity {
    Button submit;
    File PhotoPath;
    String Name,Address,Date,Customer_Ref,Note,Trip,Contact,Mobile;
    ImageView iv1,iv2,iv3,iv4,capimg;
    TextView txtcustomer,txtaddress,txtdate,txttime,txtnote,txttrip;
    File Path;
    String encoden=null;
    String CurImagePath;
    String REGISTER_URL;
    private String jsonResponse;
    String imageString,imagestring2,imagestring3,imagestring4;
    Blob im;
    Date sdate;
    int i=1;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 0;
    public static final String MyPREFERENCES = "MyPrefs";
    private SharedPreferences numberPreferences;
    private SharedPreferences.Editor numberPrefsEditor;
    private static final String TAG = "BOOMBOOMTESTGPS";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        TextView title=(TextView)findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        ImageView img=(ImageView)findViewById(getResources().getIdentifier("imageView", "id", getPackageName()));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout l = new logout(DeliveryDetail.this);
                l.signOut();

                SharedPreferences sharedpreferences = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
            }
        });
        title.setText("MY DELIVERY");
        bottomNavigationView.getMenu().getItem(1).setChecked(true);
        submit = (Button) findViewById(R.id.btnSubmit);
        iv1 = (ImageView) findViewById(R.id.iv1);
        iv2 = (ImageView) findViewById(R.id.iv2);
        iv3 = (ImageView) findViewById(R.id.iv3);
        iv4 = (ImageView) findViewById(R.id.iv4);
        capimg = (ImageView) findViewById(R.id.photo);
        txtcustomer=(TextView)findViewById(R.id.customer);
        txtaddress=(TextView)findViewById(R.id.address);
//        txtdate=(TextView)findViewById(R.id.date);
//        txttime=(TextView)findViewById(R.id.time);
        txtnote=(TextView)findViewById(R.id.deliveryno);
        txttrip=(TextView)findViewById(R.id.trip);
        Intent intent = getIntent();
        Name = intent.getStringExtra("n");
        Address=intent.getStringExtra("a");
        Customer_Ref=intent.getStringExtra("r");
        Date=intent.getStringExtra("d");
        Note=intent.getStringExtra("note");
        Trip=intent.getStringExtra("trip");
        Contact=intent.getStringExtra("contact");
        Mobile=intent.getStringExtra("mobile");
        numberPreferences = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        numberPrefsEditor = numberPreferences.edit();
        numberPrefsEditor.putString("con", Contact);
        numberPrefsEditor.putString("mob", Mobile);
        numberPrefsEditor.putString("cust", Customer_Ref);
        numberPrefsEditor.commit();
        txtcustomer.setText(Customer_Ref);
        txtaddress.setText(Address);
//        txtdate.setText(Date);
       // txttime.setText(Customer_Ref);
        txtnote.setText(Note);
        txttrip.setText(Trip);
//        txtcustomer.setText(getIntent().getSerializableExtra("n").toString());
//        txtaddress.setText(getIntent().getSerializableExtra("a").toString());
//        txttime.setText(getIntent().getSerializableExtra("r").toString());
//         txtdate.setText(getIntent().getSerializableExtra("d").toString());
        File root = new File(Environment.getExternalStorageDirectory() + "/HafarySys/");
        root.mkdirs();
        final File Photos = new File(Environment.getExternalStorageDirectory() + "/HafarySys/Photos/");
        Photos.mkdirs();
        Path = new File(String.format(Environment.getExternalStorageDirectory() + "/HafarySys/Photos/"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UploadImage();
            }
        });
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
        capimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (iv1.getDrawable() == null || iv2.getDrawable() == null || iv3.getDrawable() == null || iv4.getDrawable() == null) {
                   // Path = new File(String.format(Environment.getExternalStorageDirectory() + "/HafarySys/Photos/%s.jpg", "Photo_D_" + /*Dname +*/ "_" + i));
                    Intent startCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


                    if (ActivityCompat.checkSelfPermission(DeliveryDetail.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                   // startCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(Path));
                    startActivityForResult(startCamera, 2);
                } else {
                    Toast.makeText(getApplicationContext(), "You have cliked max pics for this record", LENGTH_LONG).show();
                    return;
                }


            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return  true;
//    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK ) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inJustDecodeBounds = true;
           // options.inSampleSize = 3;
           // String demopath = String.format(Environment.getExternalStorageDirectory() + "/HafarySys/Photos/%s.jpg", "Photo_D_" +/* Dname + */"_" + i);
           // Bitmap b = BitmapFactory.decodeFile(Path.getAbsolutePath());
            Bitmap b = (Bitmap) data.getExtras().get("data");
//            int h=b.getHeight();
//            int w=b.getWidth();
//            h=h/2;
//            w=w/2;
//            final Bitmap newbitMap = Bitmap.createScaledBitmap(b, h, w, true);
            if (iv1.getDrawable() == null) {
                try {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    b.compress(Bitmap.CompressFormat.PNG, 50, baos);
                    byte[] imageBytes = baos.toByteArray();
//                    Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0,
//                            imageBytes.length);
                    imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    iv1.setImageBitmap(b);
                }catch(Exception e){};
               // iv1.setImageURI(Uri.fromFile(Path));

                //CurImagePath = demopath;
                //i = 1;
                //i++;
            } else if (iv2.getDrawable() == null) {
               // iv2.setImageURI(Uri.fromFile(Path));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 50, baos);
                byte[] imageBytes = baos.toByteArray();
                 b = BitmapFactory.decodeByteArray(imageBytes, 0,
                        imageBytes.length);
                imagestring2 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                //im=(Blob)imageString;
                iv2.setImageBitmap(b);

                //CurImagePath = demopath;
                //i = 2;
                //i++;
            } else if (iv3.getDrawable() == null) {
                //iv3.setImageURI(Uri.fromFile(Path));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 50, baos);
                byte[] imageBytes = baos.toByteArray();
                Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0,
                        imageBytes.length);
                imagestring3 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                iv3.setImageBitmap(bitmap);

                //CurImagePath = demopath;
               // i = 3;
               // i++;
            }else if (iv4.getDrawable() == null) {
                //iv3.setImageURI(Uri.fromFile(Path));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 50, baos);
                byte[] imageBytes = baos.toByteArray();
                Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0,
                        imageBytes.length);
                imagestring4 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                iv4.setImageBitmap(bitmap);

                //CurImagePath = demopath;
                // i = 3;
                // i++;
            }
        }
    }
    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
    public void onDestroy() {
        super.onDestroy();
        unbindDrawables(findViewById(R.id.iv1));
        unbindDrawables(findViewById(R.id.iv2));
        unbindDrawables(findViewById(R.id.iv3));
        unbindDrawables(findViewById(R.id.iv4));
        System.gc();
        iv1.setImageDrawable(null);
        iv2.setImageDrawable(null);
        iv3.setImageDrawable(null);
        iv4.setImageDrawable(null);
    }
    private void UploadImage() {
       // String n="DSCH00028";

        String encodeimg=null;
        try{
            //encodeimg = URLEncoder.encode(imageString,"UTF-8");
            encoden = URLEncoder.encode(Name,"UTF-8");
        }catch(Exception e){};
        REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_img_in_delivery_schedule";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Error:" + error.toString());
                        updateDatabaseImg();
                    }
                })
        {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name",encoden );
                if (iv2.getDrawable() == null && iv3.getDrawable() == null && iv4.getDrawable() == null) {
                    params.put("img_1", imageString);
                }else if(iv3.getDrawable() == null && iv4.getDrawable() == null) {
                    params.put("img_1", imageString);
                    params.put("img_2", imagestring2);
                }else if(iv4.getDrawable() == null) {
                    params.put("img_1", imageString);
                    params.put("img_2", imagestring2);
                    params.put("img_3", imagestring3);
                }else {
                    params.put("img_1", imageString);
                    params.put("img_2", imagestring2);
                    params.put("img_3", imagestring3);
                    params.put("img_4", imagestring4);
                }
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                70000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if(id==R.id.logout)
//        {
//            logout l = new logout(this);
//            l.signOut();
//            SharedPreferences sharedpreferences = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.clear();
//            editor.commit();
//        }
//        return super.onOptionsItemSelected(item);
//
//    }
public  void updateDatabaseImg(){
    GPSDatabase myDatabase=new GPSDatabase(getApplicationContext());

    try {
        myDatabase.open();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    if (iv2.getDrawable() == null && iv3.getDrawable() == null && iv4.getDrawable() == null) {
//        try {
//            byte[] byteData = imageString.getBytes("UTF-8");//Be
//            // tter to specify encoding
//
//        Blob blobData = myDatabase.createBlob();
//        blobData.setBytes(1, byteData);
//        }catch (Exception e){}
        byte[] decodedByte = Base64.decode(imageString, 0);
        myDatabase.insertimgRow(Name,decodedByte,null,null,null);
    } else if(iv3.getDrawable() == null && iv4.getDrawable() == null) {
        byte[] decodedByte = Base64.decode(imageString, 0);
        myDatabase.insertimgRow(Name,decodedByte,imagestring2,null,null);
    }else if(iv4.getDrawable() == null) {
        byte[] decodedByte = Base64.decode(imageString, 0);
        myDatabase.insertimgRow(Name,decodedByte,imagestring2,imagestring2,null);
    }else{
        byte[] decodedByte = Base64.decode(imageString, 0);
        myDatabase.insertimgRow(Name,decodedByte,imagestring2,imagestring3,imagestring4);
    }
//    myDatabase.insertimgRow(imageString,imagestring2,imagestring3,imagestring4);
    myDatabase.close();
}
}
