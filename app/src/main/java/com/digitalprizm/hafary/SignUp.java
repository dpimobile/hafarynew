package com.digitalprizm.hafary;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

public class SignUp extends AppCompatActivity {
    private TextView txterr;
    private com.android.volley.RequestQueue mQueue;
    private AbstractHttpClient mHttpClient;
    private EditText etfullname,etemail;
    String FullName,Email,encodeemail,encodefullname;
    RelativeLayout rl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        TextView title = (TextView) findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        title.setText("REGISTER");
        mHttpClient = new DefaultHttpClient();
        mQueue = Volley.newRequestQueue(SignUp.this, new HttpClientStack(mHttpClient));
        Button btnRequest = (Button) findViewById(R.id.btnsignup);
        etfullname=(EditText)findViewById(R.id.fullname);
        etemail=(EditText)findViewById(R.id.email);
        txterr=(TextView)findViewById(R.id.errtxt);
        rl=(RelativeLayout)findViewById(R.id.activity_sign_up);
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etfullname.getText().toString().equals(""))
                {
                    //Toast.makeText(getApplicationContext(),"Email cannot be blank", LENGTH_LONG).show();
                    etfullname.setError("FullName required !");
                }

                else if(etemail.getText().toString().equals(""))
                {
                    //Toast.makeText(getApplicationContext(),"Password cannot be blank", LENGTH_LONG).show();
                    etemail.setError("Email required !");
                }
                else{
                FullName=etfullname.getText().toString();
                Email=etemail.getText().toString();
                //mQueue.add(createRequest());
                new DataAsync("").execute();}
            }
        });
    }

    private StringRequest createRequest() {
        try {
            encodeemail = URLEncoder.encode(Email, "UTF-8");
            encodefullname = URLEncoder.encode(FullName, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringRequest myReq = new StringRequest(Request.Method.POST,
                "http://hafarydev.digitalprizm.net/api/method/frappe.core.doctype.user.user.sign_up?email=" + encodeemail + "&full_name=" + encodefullname + "&redirect_to=0",
                createMyReqSuccessListener(),
                createMyReqErrorListener());

        return myReq;
    }


    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CookieStore cs = mHttpClient.getCookieStore();
                BasicClientCookie c = (BasicClientCookie) getCookie(cs, "my_cookie");
                Toast.makeText(SignUp.this,"Email has been sent to set password !!",Toast.LENGTH_LONG).show();
//                Intent i=new Intent(SignUp.this,Login.class);
//                startActivity(i);
            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // setTvCookieText(error.getMessage());
                //Toast.makeText(SignUp.this,error.getMessage(),Toast.LENGTH_LONG).show();
                txterr.setText("Something went wrong. Try again !!");

            }
        };
    }
    public Cookie getCookie(CookieStore cs, String cookieName) {
        Cookie ret = null;

        List<Cookie> l = cs.getCookies();
        for (Cookie c : l) {
            if (c.getName().equals(cookieName)) {
                ret = c;
                break;
            }
        }

        return ret;
    }
    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        String DownloadWithError= null;
        public String SOAP_ADDRESS = "";
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }

            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(DownloadWithError == "Please check internet connection")
            {
                // Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
                Snackbar snackbar=Snackbar.make(rl,"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataAsync("").execute();
                    }
                });
                snackbar.setActionTextColor(Color.RED);
                View sbview=snackbar.getView();
                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.YELLOW);
                snackbar.show();
            }
            else
            {
                mQueue.add(createRequest());

            }

        }
    }
}
