package com.digitalprizm.hafary;

/**
 * Created by Dell on 26-Apr-17.
 */

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

public class ForgotPass extends AppCompatActivity {
    private TextView textlogin;
    RelativeLayout rl;
    private com.android.volley.RequestQueue mQueue;
    private AbstractHttpClient mHttpClient;
    private EditText etemail;
    String Email;String encodeUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        TextView title = (TextView) findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        title.setText("RESET PASSWORD");
        mHttpClient = new DefaultHttpClient();

        mQueue = Volley.newRequestQueue(ForgotPass.this, new HttpClientStack(mHttpClient));
        Button btnRequest = (Button) findViewById(R.id.btnforgotpassword);
        rl=(RelativeLayout)findViewById(R.id.ral);
        textlogin=(TextView)findViewById(R.id.txtlogin);
        etemail=(EditText)findViewById(R.id.etmail);
        textlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ForgotPass.this,Login.class);
                startActivity(i);
            }
        });
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etemail.getText().toString().equals(""))
                {
                    //Toast.makeText(getApplicationContext(),"Email cannot be blank", LENGTH_LONG).show();
                    etemail.setError("Email Id required !");
                }
                else{
                Email=etemail.getText().toString();
                new DataAsync("").execute();}
                //mQueue.add(createRequest());
            }
        });
    }
    private StringRequest createRequest() {
        try {
            encodeUrl = URLEncoder.encode(Email, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringRequest myReq = new StringRequest(Request.Method.POST,
                "http://hafarydev.digitalprizm.net/api/method/frappe.core.doctype.user.user.reset_password?user=" + encodeUrl,
                createMyReqSuccessListener(),
                createMyReqErrorListener());

        return myReq;
    }


    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CookieStore cs = mHttpClient.getCookieStore();
                BasicClientCookie c = (BasicClientCookie) getCookie(cs, "my_cookie");
                String res="";
                if (response != null && response.contains("message")) {
                    try {
                        JSONObject jsonObj = new JSONObject(response);
                        res = jsonObj.getString("message");

//                            if(res.equals("Password reset instructions have been sent to your email"))
//                            {
//                                Toast.makeText(getApplicationContext(),"Email has been sent to reset password !!!", LENGTH_LONG).show();
//
//                            }
                        if(res.equals("not found")){
                            Toast.makeText(getApplicationContext(),"Please enter valid Email Id", LENGTH_LONG).show();
                        }
                    }catch (Exception e){}}
                if (response != null && response.contains("_server_messages")) {

                    Toast.makeText(getApplicationContext(), "Email has been sent to reset password !!!", LENGTH_LONG).show();

                }

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // setTvCookieText(error.getMessage());
                Toast.makeText(ForgotPass.this,"something went wrong !!!", LENGTH_LONG).show();
                //txterr.setText("Something went wrong. Try again !!");
            }
        };
    }
    public Cookie getCookie(CookieStore cs, String cookieName) {
        Cookie ret = null;

        List<Cookie> l = cs.getCookies();
        for (Cookie c : l) {
            if (c.getName().equals(cookieName)) {
                ret = c;
                break;
            }
        }

        return ret;
    }
    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        String DownloadWithError= null;
        public String SOAP_ADDRESS = "";
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }

            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(DownloadWithError == "Please check internet connection")
            {
                // Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
                Snackbar snackbar=Snackbar.make(rl,"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataAsync("").execute();
                    }
                });
                snackbar.setActionTextColor(Color.RED);
                View sbview=snackbar.getView();
                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.YELLOW);
                snackbar.show();
            }
            else
            {
                mQueue.add(createRequest());

            }

        }
    }
}
