package com.digitalprizm.hafary;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class BaseActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    BottomNavigationView bottomNavigationView;
    private NavigationView navigationView;
    private RelativeLayout fullLayout;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private int selectedNavItemId;
    public static final int  MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;
    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        /**
         * This is going to be our actual root layout.
         */
        fullLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.activity_main, null);
        /**
         * {@link FrameLayout} to inflate the child's view. We could also use a {@link android.view.ViewStub}
         */
        FrameLayout activityContainer = (FrameLayout) fullLayout.findViewById(R.id.frame_layout);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);

        /**
         * Note that we don't pass the child's layoutId to the parent,
         * instead we pass it our inflated layout.
         */
        super.setContentView(fullLayout);
        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        // = (Toolbar) findViewById(R.id.toolbar);


//        if (useToolbar())
//        {
//            setSupportActionBar(toolbar);
//        }
//        else
//        {
//            toolbar.setVisibility(View.GONE);
//        }
//
        setUpNavView();
    }

    /**
     * Helper method that can be used by child classes to
     * specify that they don't want a {@link Toolbar}
     * @return true
     */
//    protected boolean useToolbar()
//    {
//        return true;
//    }
    protected void setUpNavView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

//        if( useDrawerToggle()) { // use the hamburger menu
//            drawerToggle = new ActionBarDrawerToggle(this, fullLayout, toolbar,
//                    R.string.nav_drawer_opened,
//                    R.string.nav_drawer_closed);
//
//            fullLayout.setDrawerListener(drawerToggle);
//            drawerToggle.syncState();
//        } else if(useToolbar() && getSupportActionBar() != null) {
//            // Use home/back button instead
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setHomeAsUpIndicator(getResources()
//                    .getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha));
//        }
    }

    /**
     * Helper method to allow child classes to opt-out of having the
     * hamburger menu.
     *
     * @return
     */
//    protected boolean useDrawerToggle()
//    {
//        return true;
//    }
//
//
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
//        fullLayout.closeDrawer(GravityCompat.START);
        selectedNavItemId = menuItem.getItemId();

        return onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        int seid = 0;
        // Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.Call:
                seid = 3;
                Intent ii = new Intent(BaseActivity.this, AboutActivity.class);
                startActivity(ii);
//                String number = "08378982882";
//
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    //return true;
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                            Manifest.permission.CALL_PHONE)) {
//
//                        // Show an explanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//
//                    } else {
//
//                        // No explanation needed, we can request the permission.
//
//                        ActivityCompat.requestPermissions(this,
//                                new String[]{Manifest.permission.CALL_PHONE},
//                                MY_PERMISSIONS_REQUEST_CALL_PHONE);
//
//                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                        // app-defined int constant. The callback method gets the
//                        // result of the request.
//                    }
//
//                }
//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:" + number));
//                startActivity(callIntent);
//                bottomNavigationView.getMenu().getItem(seid).setChecked(true);
//                Intent ii = new Intent(BaseActivity.this, Login.class);
//                startActivity(ii);

                //    selectedFragment = ItemOneFragment.newInstance();
                break;
            case R.id.Profile:
                seid = 0;
                bottomNavigationView.getMenu().getItem(seid).setChecked(true);
                //selectedFragment = ItemTwoFragment.newInstance();
                Intent i = new Intent(BaseActivity.this, Driver_Det.class);
                startActivity(i);
                break;
            case R.id.Driver:
                seid = 1;
                bottomNavigationView.getMenu().getItem(seid).setChecked(true);
                Intent iii = new Intent();
                iii.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                iii.setClassName(this,"com.digitalprizm.hafary.DeliveryList");
                startActivity(iii);
                //  selectedFragment = ItemThreeFragment.newInstance();
                break;
//            case R.id.Sms:
//                seid = 3;
//                bottomNavigationView.getMenu().getItem(seid).setChecked(true);
//                Intent iColl = new Intent(BaseActivity.this, SmsActivity.class);
//                startActivity(iColl);
//                //  selectedFragment = ItemThreeFragment.newInstance();
//                break;
            case R.id.Scan:
                seid = 2;
                Intent iscan = new Intent(BaseActivity.this, Scan.class);
                startActivity(iscan);
                //  selectedFragment = ItemThreeFragment.newInstance();
                break;

        }
        return super.onOptionsItemSelected(item);
    }}
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return ;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//}}