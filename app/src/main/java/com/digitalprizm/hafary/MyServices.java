package com.digitalprizm.hafary;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.impl.client.DefaultHttpClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;



public class MyServices extends Service {
    private static final String TAG = "BOOMBOOMTESTGPS";
    public static final String MyPREFERENCES = "MyPrefs";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 0f;
    String strlat,strlon,stopstrlat,stopstrlon, Name;Boolean flag;
    private static Handler handler;
    Handler mHandler=new Handler();
    String REGISTER_URL,Login_URL,UserID,pass,Start_URL,Stop_URL;
    Location mLastLocation;
    private class LocationListener implements android.location.LocationListener {
        Double lat1,lon1;
        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }
        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            lat1 = mLastLocation.getLatitude();
            lon1 = mLastLocation.getLongitude();
//            SharedPreferences myPrefs = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
//            String usr = myPrefs.getString("vehicalno.", "DEFAULT VALUE");
            strlat = String.valueOf(lat1);
            strlon=String.valueOf(lon1);
            String encodlat= null;
            String encodlon=null;
            String encod=null;
            String encoduser=null;
            try {
                encodlat = URLEncoder.encode(strlat,"UTF-8");
                encodlon = URLEncoder.encode(strlon,"UTF-8");
             //encod=URLEncoder.encode(usr,"UTF-8");
                encoduser=URLEncoder.encode(Name,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
           if(flag==true){
               Start_URL="http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_start_loc_in_ds?name=" + encoduser + "&lat=" + encodlat + "&lon=" + encodlon;
               StartUser();
               flag=false;
           }
            REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_driving_in_ds?name=" + encoduser +"&lat=" + encodlat + "&lon=" + encodlon;
            //REGISTER_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.delivery_management.doctype.carrier.carrier_api.update_carrier_location?name=" + encod +"&lat=" + encodlat + "&lon=" + encodlon;
            Toast.makeText(getApplicationContext(),"Latitude:"+lat1+"\nLongitude:"+lon1,Toast.LENGTH_LONG).show();
            registerUser();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }
    public MyServices() {
        super();
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        flag=true;
        Name = intent.getStringExtra("n");
        final int currentId = startId;

        Runnable r = new Runnable() {
            public void run() {

                while(true) {

                    long endTime = System.currentTimeMillis() + 60 * 1000;
                    while (System.currentTimeMillis() < endTime) {
                        synchronized (this) {
                            try {
                                wait(endTime - System.currentTimeMillis());
                            } catch (Exception e) {
                            }
                        }
                    }

                }

            }
        };

        Thread t = new Thread(r);
        t.start();

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");


        initializeLocationManager();

        SharedPreferences mPrefs = getApplicationContext().getSharedPreferences(Login.MyPREFERENCES, MODE_PRIVATE);
        UserID = mPrefs.getString("usercode", "DEFAULT VALUE");
        pass = mPrefs.getString("password", "DEFAULT VALUE");
        Login_URL = "http://hafarydev.digitalprizm.net/api/method/login?usr=" + UserID + "&pwd="+pass;
        singleClass.mHttpClient = new DefaultHttpClient();
        final Handler handler = new Handler();

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    private void registerUser(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                      //  Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        updateDatabase();
                        //Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void StartUser(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Start_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                        updateDatabaseStart();
                    }
                }){
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void StopUser(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Stop_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                        updateDatabaseStop();
                    }
                }){
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        flag = true;
        Location lastKnownLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (lastKnownLocation != null) {
            double lat = lastKnownLocation.getLatitude();
            double lon = lastKnownLocation.getLongitude();
            stopstrlat = String.valueOf(lat);
            stopstrlon = String.valueOf(lon);
            String encodlat = null;
            String encodlon = null;
            String encoduser = null;
            try {
                encodlat = URLEncoder.encode(stopstrlat, "UTF-8");
                encodlon = URLEncoder.encode(stopstrlon, "UTF-8");
                encoduser = URLEncoder.encode(Name, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            super.onDestroy();
            Stop_URL = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.update_stop_loc_in_ds?name=" + encoduser + "&lat=" + encodlat + "&lon=" + encodlon;
            StopUser();
            if (mLocationManager != null) {
                for (int i = 0; i < mLocationListeners.length; i++) {
                    try {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mLocationManager.removeUpdates(mLocationListeners[i]);

                    } catch (Exception ex) {
                        Log.i(TAG, "fail to remove location listners, ignore", ex);
                    }
                }
            }
        }
    }
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }

    }

    public  void updateDatabase(){
        GPSDatabase myDatabase=new GPSDatabase(getApplicationContext());

        try {
            myDatabase.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String Status="0";
        myDatabase.insertRows(strlat.substring(0,4),strlon.substring(0,4),Name,Status);
        myDatabase.close();
    }
    public  void updateDatabaseStart(){
        GPSDatabase myDatabase=new GPSDatabase(getApplicationContext());

        try {
            myDatabase.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String Status="1";
        myDatabase.insertRows(strlat.substring(0,4),strlon.substring(0,4),Name,Status);
        myDatabase.close();
    }
    public  void updateDatabaseStop(){
        GPSDatabase myDatabase=new GPSDatabase(getApplicationContext());

        try {
            myDatabase.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String Status="2";
        myDatabase.insertRows(stopstrlat.substring(0,4),stopstrlon.substring(0,4),Name,Status);
        myDatabase.close();
    }
}