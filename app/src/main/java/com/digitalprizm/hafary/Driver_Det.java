package com.digitalprizm.hafary;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.widget.Toast.LENGTH_LONG;


public class Driver_Det extends BaseActivity {
    String Email;
    RelativeLayout rl;
   // private String urlJsonObj = "http://hafarydev.digitalprizm.net/api/method/delivery_management.delivery_management.doctype.driver.driver_api.get_driver_details?user_id=neha@digitalprizm.net";
    String urlJsonObj;
    private SharedPreferences driverPreferences;
    private SharedPreferences.Editor driverPrefsEditor;
    public static final String MyPREFERENCES = "Prefs";
   // private final static String IMAGE_URL = "http://hafarydev.digitalprizm.net/api/resource/Driver/?filters=Driver,name=DRV-001&&fields=profile_picture";
    private TextView txtname,txtemail,txtdriverid,txtvehicalno,txtvehicaltype;
    private static String TAG = Driver_Det.class.getSimpleName();
    private String jsonResponse;
    ImageView img;
    String UserID,pass;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_driver_details);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setIcon(R.drawable.hafary);
        TextView title=(TextView)findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        ImageView img=(ImageView)findViewById(getResources().getIdentifier("imageView", "id", getPackageName()));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout l = new logout(Driver_Det.this);
                l.signOut();

            SharedPreferences sharedpreferences = getSharedPreferences(Login.SysPrefs, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear();
            editor.commit();
            }
        });
        title.setText("MY PROFILE");

        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        rl=(RelativeLayout)findViewById(R.id.activity_driver);
        //img=(ImageView) findViewById(R.id.imageView);
        //txtname = (TextView) findViewById(R.id.Name);
        txtname = (TextView) findViewById(R.id.etname);
        txtemail = (TextView) findViewById(R.id.etemail);
        txtdriverid = (TextView) findViewById(R.id.etdriverid);
        txtvehicalno = (TextView) findViewById(R.id.etvehicalno);
        txtvehicaltype = (TextView) findViewById(R.id.etvehicaltype);
        driverPreferences = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        driverPrefsEditor = driverPreferences.edit();
        SharedPreferences myPrefs = this.getSharedPreferences(Login.MyPREFERENCES, MODE_PRIVATE);
        UserID = myPrefs.getString("usercode", "DEFAULT VALUE");

//        Intent i = getIntent();
//       Email = i.getStringExtra("name");
        urlJsonObj="http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.get_driver_details_from_email?user_id=" + UserID;
        new DataAsync("").execute();
       // makeJsonObjectRequest();
       // getImageFromURL();
    }

    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj , "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject msg = response.getJSONObject("message");
                    String name = msg.getString("Full Name");
                    String imageurl = msg.getString("Profile Picture");
                    String image = "http://hafarydev.digitalprizm.net/" + imageurl;
                    String vehicalno = msg.getString("Assigned Vehicle No.");
                    String Carrier_type = msg.getString("carrier_type");
                    String email = msg.getString("Email ");
                    String driverid = msg.getString("ID");
                    txtname.setText(name);
                    txtemail.setText(email);
                    txtdriverid.setText(driverid);
                    txtvehicalno.setText(vehicalno);
                    txtvehicaltype.setText(Carrier_type);
//                    jsonResponse = "";
//                    jsonResponse += "Full Name    : " + name + "\n\n";
//                    jsonResponse += "Email Id        : " + email + "\n\n";
//                    jsonResponse += "Driver Id        : " + driverid + "\n\n";
//                    jsonResponse += "Vehical No.   : " + vehicalno + "\n\n";
//                    jsonResponse += "Vehical Type : " + Carrier_type + "\n\n";
                   // txtname.setText(jsonResponse);
//                    if (imageurl == "null") {
//                        img.setBackgroundResource(R.drawable.user_);
//                    } else  {
//                        Picasso.with(Driver_Det.this).load(image).into(img);
//                    }

                   // if (msg.has("Profile Picture"))
                    driverPrefsEditor.putString("vehicalno.", email);
                    driverPrefsEditor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Something went wrong. Try again !!",
                            LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Something went wrong. Try again !!", Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        String DownloadWithError= null;
        public String SOAP_ADDRESS = "";
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }

            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(DownloadWithError == "Please check internet connection")
            {
                 Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
//               // Snackbar snackbar=Snackbar.make(getView(),"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new DataAsync("").execute();
//                    }
//                });
//                snackbar.setActionTextColor(Color.RED);
//                View sbview=snackbar.getView();
//                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
//                tv.setTextColor(Color.YELLOW);
//                snackbar.show();

            }
            else
            {
                makeJsonObjectRequest();

            }

        }
    }

//            public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return  true;
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//
//        if(id==R.id.logout)
//        {
//            logout l = new logout(this);
//            l.signOut();
//
//            SharedPreferences sharedpreferences = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.clear();
//            editor.commit();
//
//        }
//        return super.onOptionsItemSelected(item);
//
//    }
}
