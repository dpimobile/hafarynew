package com.digitalprizm.hafary;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;


public class CollectionList extends BaseActivity {

    static final String TAG = "DeliveryDetails";

    ListView lv;
    ArrayList<HashMap<String, String>> contactList;
    ArrayList<HashMap<String, String>> productList;
    ListAdapter adapter;
    SQLiteDatabase db;

    BasicClientCookie c =null;
    String a[]= {"name","lat","lon","customer"};
    String name;
    String strPost,strPut;
    String UserID,pass;
    boolean postData = false;
    String lat,lon,idDEL;
    private String jsonResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.main);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.actionbar);
            TextView title=(TextView)findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
            title.setText("COLLECTION");

            bottomNavigationView.getMenu().getItem(3).setChecked(true);

            lv = (ListView) findViewById(R.id.listView);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View v,
                                        final int position, long id) {

                }
            });


            singleClass.mHttpClient = new DefaultHttpClient();

            SharedPreferences myPrefs = this.getSharedPreferences(Login.MyPREFERENCES, MODE_PRIVATE);
            UserID = myPrefs.getString("usercode", "DEFAULT VALUE");
            pass = myPrefs.getString("password", "DEFAULT VALUE");
            if (UserID != "DEFAULT VALUE" || pass != "DEFAULT VALUE") {


                new DataAsync("").execute();
            }
            contactList = new ArrayList<>();


        }  catch(
    Exception e
    )

    {

    }



    }


    private StringRequest createRequest() {
        StringRequest myReq = new StringRequest(Request.Method.POST,
                 "http://hafarydev.digitalprizm.net/api/method/login?usr=" + UserID + "&pwd="+pass,
                createMyReqSuccessListener(),
                createMyReqErrorListener());

        return myReq;
    }

/*Get data from api*/

    private StringRequest createRequest1() {
        String url="";

        try {

            url ="http://hafarydev.digitalprizm.net/api/resource/Delivery%20Order?fields="+ URLEncoder.encode("[\"customer\"]","UTF-8");

        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest myReq = new StringRequest(Request.Method.GET,
                url ,
                createMyReqSuccessListener(),
                createMyReqErrorListener());

        return myReq;
    }



   




    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;

        ProgressDialog pdLoading = new ProgressDialog(CollectionList.this);
        final AlertDialog ad=new AlertDialog.Builder(getApplicationContext()).create();
        String DownloadWithError= null;
        public String OPERATION_NAME = "GetMasterDS";
        public final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
        public String SOAP_ADDRESS = "";
        private int Cntr=0;
        public final String SOAP_ACTION = "http://tempuri.org/GetMasterDS";
        public String mstrMasterName, mstrFilter, xmlString;
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {


                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }



            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ad.setMessage("Done");

            if(DownloadWithError == "Please check internet connection")
            {
                Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
            }
            else
            {
                singleClass.mQueue.add(createRequest());
                //singleClass.mQueue.add(createPost());
            }

        }
    }




    private Response.Listener<String> createMyReqSuccessListener() {

        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CookieStore cs = singleClass.mHttpClient.getCookieStore();
               // BasicClientCookie c = (BasicClientCookie) getCookie(cs, "sid");
                c = (BasicClientCookie) getCookie(cs, "sid");
                cs.addCookie(c);
                String res="";


                 if (response != null && response.contains("data")) {
                     int i;
                    try {
                        JSONObject jsonObj = new JSONObject(response);


                        JSONArray contacts = jsonObj.getJSONArray("data");
                        int count=1;
                        // looping through All Contacts
                        for (i = 0; i < contacts.length(); i++) {

                            JSONObject c1 = contacts.getJSONObject(i);
                            String aString = Integer.toString(count);

                            String name1 = c1.getString("customer");
//                            JSONArray productitem = c1.getJSONArray("product_item");
//                            for(int j=0;j <productitem.length();j++){
//                                JSONObject c2=productitem.getJSONObject(j);
//                                String productcode=c2.getString("product_code");
//                                							jsonResponse = "";
//							jsonResponse += "Product Item: " + productcode + "\n\n";
//                                HashMap<String, String> product = new HashMap<>();
//                                product.put("product_code",jsonResponse);
//                                productList.add(product);
//
//                            }

                            HashMap<String, String> contact = new HashMap<>();


                            contact.put("customer", name1);
                            contact.put("count",aString);
                            contactList.add(contact);
                            count++;


                        }

                        adapter = new SimpleAdapter(
                                getApplicationContext(), contactList,
                                R.layout.movie_list_row, new String[]{"count", "customer"
                        }, new int[]{R.id.txt_id,R.id.title});

                       /* adapter = new ArrayAdapter<String>(this,
                                R.layout.equations_search_list, R.id.product_name, products);
                        lv.setAdapter(adapter);*/
                        lv.setItemsCanFocus(false);
                        lv.setAdapter(adapter);
                        lv.setTextFilterEnabled(true);

                    }catch (final JSONException e) {
                        //Log.e(TAG, "Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        "Json parsing error: " + e.getMessage(),
                                        LENGTH_LONG)
                                        .show();
                            }
                        });

                    }
                }

                else {

                   singleClass.mQueue.add(createRequest1());

                }

//                if (c != null) {
//
//                  //  setTvCookieText(res);
//                }

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
    }





    public Cookie getCookie(CookieStore cs, String cookieName) {
        Cookie ret = null;

        List<Cookie> l = cs.getCookies();
        for (Cookie c : l) {
            if (c.getName().equals(cookieName)) {
                ret = c;
                break;
            }
        }

        return ret;
    }


}
