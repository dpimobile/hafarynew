package com.digitalprizm.hafary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpStatus;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.widget.Toast.LENGTH_LONG;

public class Login extends AppCompatActivity {

    private static final String TAG = "Login";
    Button btnlogin;CheckBox chk;
    private SharedPreferences loginPreferences;
    private SharedPreferences sysPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private SharedPreferences.Editor SysPrefsEditor;
    EditText etname,etpwd;
    String UserID,username,pswd,pass,UsernameID,passwd;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String SysPrefs="syspref";
    String strURL;// = getResources().getString(R.string.URL);
    TextView txtforgotpassword,txtsignup,errortxt;
    RelativeLayout rl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        chk=(CheckBox)findViewById(R.id.checkBox);
        etname = (EditText) findViewById(R.id.etEmail);
        etpwd = (EditText) findViewById(R.id.etPwd);
        strURL = getResources().getString(R.string.URL);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action);
        TextView title=(TextView)findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        title.setText("LOGIN");

        loginPreferences = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();
        sysPreferences = getSharedPreferences(SysPrefs, MODE_PRIVATE);
        SysPrefsEditor = sysPreferences.edit();
        btnlogin = (Button) findViewById(R.id.btnLgin);
        rl=(RelativeLayout)findViewById(R.id.ral);
        txtforgotpassword=(TextView)findViewById(R.id.forgotpassword);
       // errortxt=(TextView)findViewById(R.id.err);
       // txtsignup=(TextView)findViewById(R.id.signup);
        txtforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Login.this,ForgotPass.class);
                startActivity(i);
            }
        });
        username = etname.getText().toString();
         pswd = etpwd.getText().toString();
        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SysPrefsEditor.putString("usercode", username);
                SysPrefsEditor.putString("password", pswd);
                SysPrefsEditor.commit();
            }
        });
        singleClass.mHttpClient = new DefaultHttpClient();

        singleClass.mQueue = Volley.newRequestQueue(Login.this, new HttpClientStack(singleClass.mHttpClient));

        SharedPreferences myPrefs = this.getSharedPreferences(Login.MyPREFERENCES, MODE_PRIVATE);
        UserID = myPrefs.getString("usercode", "DEFAULT VALUE");
        pass = myPrefs.getString("password", "DEFAULT VALUE");

        SharedPreferences sPrefs = this.getSharedPreferences(Login.SysPrefs, MODE_PRIVATE);
        UsernameID = sPrefs.getString("usercode", "DEFAULT VALUE");
        passwd = sPrefs.getString("password", "DEFAULT VALUE");
        if(UsernameID != "DEFAULT VALUE" || passwd != "DEFAULT VALUE") {

            Intent intent = new Intent(this, Driver_Det.class);
            intent.putExtra("name",UserID);
            startActivity(intent);
            finish();

        }
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(etname.getText().toString().equals(""))
                {
                    //Toast.makeText(getApplicationContext(),"Email cannot be blank", LENGTH_LONG).show();
                    etname.setError("Email Id required !");
                }

                else if(etpwd.getText().toString().equals(""))
                {
                    //Toast.makeText(getApplicationContext(),"Password cannot be blank", LENGTH_LONG).show();
                    etpwd.setError("Password required !");
                }
                else{
                UserID = etname.getText().toString();
                pass = etpwd.getText().toString();
                new DataAsync("").execute();}

            }
        });

    }

    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }


    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        String DownloadWithError= null;
        public String SOAP_ADDRESS = "";
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }

            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(DownloadWithError == "Please check internet connection")
            {
               // Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
                Snackbar snackbar=Snackbar.make(rl,"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataAsync("").execute();
                    }
                });
                snackbar.setActionTextColor(Color.RED);
                View sbview=snackbar.getView();
                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.YELLOW);
                snackbar.show();
            }
            else
            {
                singleClass.mQueue.add(createRequest());

            }

        }
    }

    public StringRequest createRequest() {
        String post = strURL + "api/method/login?usr=" + UserID + "&pwd="+pass;


        StringRequest myReq = new StringRequest(Request.Method.POST,
                post,
                createMyReqSuccessListener(),
                createMyReqErrorListener());

        return myReq;
    }

    private Response.Listener<String> createMyReqSuccessListener() {

        return new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CookieStore cs = singleClass.mHttpClient.getCookieStore();

                //singleClass.c = (BasicClientCookie) getCookie(cs, "sid");
                cs.addCookie(singleClass.c);
                String res="";
                if (response != null && response.contains("message")) {
                    try {

                        String name = etname.getText().toString();
                        String pwd = etpwd.getText().toString();
                        loginPrefsEditor.putString("usercode", name);
                        loginPrefsEditor.putString("password", pwd);
                        loginPrefsEditor.commit();
                        JSONObject jsonObj = new JSONObject(response);

                        res = jsonObj.getString("message");
                        if(res.equals("Logged In") || res.equals("No App"))
                        {
                           // Toast.makeText(getApplicationContext(),"Login successful", LENGTH_LONG).show();
                            Intent i = new Intent(Login.this,Driver_Det.class);
                            i.putExtra("username",UserID);
                            startActivity(i);
                            finish();
                        }

                    }catch (final JSONException e) {
                        Log.e(TAG, "Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        " something went wrong!!! " ,
                                        LENGTH_LONG)
                                        .show();
         //                       errortxt.setText("Something went wrong. Try again !!");
                            }
                        });

                    }
                }

                else {

                    Toast.makeText(getApplicationContext(),"Something went wrong!!!", LENGTH_LONG).show();
           //         errortxt.setText("Something went wrong.Try again !!");

                }


            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.statusCode == HttpStatus.SC_UNAUTHORIZED) {
                    // HTTP Status Code: 401 Unauthorized
                    Toast.makeText(getApplicationContext(),"Please enter Email Id and Password !!",LENGTH_LONG).show();
                }

             //   errortxt.setText("Something wrong. Try again!!");
            }
        };
    }

//    public Cookie getCookie(CookieStore cs, String cookieName) {
//        Cookie ret = null;
//
//        List<Cookie> l = cs.getCookies();
//        for (Cookie c : l) {
//            if (c.getName().equals(cookieName)) {
//                ret = c;
//                break;
//            }
//        }
//
//        return ret;
//    }
}
