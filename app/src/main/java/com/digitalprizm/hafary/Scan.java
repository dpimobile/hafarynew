package com.digitalprizm.hafary;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Scan extends BaseActivity {
    String substr;
    String name;
    Date date;
    String address;
    String custref;
    String Customer;
    String Address;
    String REGISTER_URL;
    String encod;
    String shippingdate,Note,Trip;
   String reportDate,emailid,UserID;
    TextView Scan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        TextView title=(TextView)findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        ImageView img=(ImageView)findViewById(getResources().getIdentifier("imageView", "id", getPackageName()));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout l = new logout(Scan.this);
                l.signOut();

                SharedPreferences sharedpreferences = getSharedPreferences(Login.SysPrefs, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
            }
        });
        title.setText("COLLECTION");
//        address=getIntent().getSerializableExtra("a").toString();
//        custref=getIntent().getSerializableExtra("r").toString();
//        date=getIntent().getSerializableExtra("d").toString();
        bottomNavigationView.getMenu().getItem(2).setChecked(true);
//        Scan=(TextView)findViewById(R.id.scan);

        SharedPreferences myPrefs = this.getSharedPreferences(Login.MyPREFERENCES, MODE_PRIVATE);
        UserID = myPrefs.getString("usercode", "DEFAULT VALUE");
    }

    public void scanBarcode(View view) {
        new IntentIntegrator(this).initiateScan();
    }

    /*public void scanBarcodeInverted(View view){
        *//*IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.addExtra(Intents.Scan.INVERTED_SCAN, true);
        integrator.initiateScan();*//*

        MailService mailer = new MailService("runita@digitalprizm.net","neha@digitalprizm.net","Subject","hi", "hello");
        try {
            mailer.sendAuthenticated();
        } catch (Exception e) {
            Log.e("error", "Failed sending email.", e);
        }
    }*/



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {

                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {

                String contents = data.getStringExtra("SCAN_RESULT");
//                Scan.setText(contents);
//                Toast.makeText(this, "Scanned: " + contents, Toast.LENGTH_LONG).show();
                // substr=contents.substring(15,24);
                int a = contents.indexOf("DSCH");
                substr=contents.substring(a,14);
                //substr=contents.substring(5,14);
                try {

                    encod=URLEncoder.encode(substr,"UTF-8");

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                REGISTER_URL="http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.get_single_delivery_shedule?name=" + encod;
                makeJsonObjectRequest();
                //Toast.makeText(this, "" + substr, Toast.LENGTH_LONG).show();
                if(contents.startsWith("BEGIN:")){

                    String[] tokens = contents.split("\n");

                    for (int i = 0; i < tokens.length; i++) {
                        System.out.println(" " + tokens[i]);

                        if (tokens[i].startsWith("BEGIN:")) {
                            String Type = tokens[i].substring(6);
                            Toast.makeText(getApplicationContext(),Type,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("N:")) {
                            String Name = tokens[i].substring(2);
                            Toast.makeText(getApplicationContext(),Name,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("ORG:")) {
                            String Org = tokens[i].substring(4);
                            Toast.makeText(getApplicationContext(),Org,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("TEL:")) {
                            String Tel = tokens[i].substring(4);
                            Toast.makeText(getApplicationContext(),Tel,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("URL:")) {
                            String Url = tokens[i].substring(4);
                            Toast.makeText(getApplicationContext(),Url,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("EMAIL:")) {
                            String Email = tokens[i].substring(6);
                            Toast.makeText(getApplicationContext(),Email,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("ADR:")) {
                            String Adr = tokens[i].substring(4);
                            Toast.makeText(getApplicationContext(),Adr,Toast.LENGTH_LONG).show();
                        } else if (tokens[i].startsWith("NOTE:")) {
                            String Note = tokens[i].substring(5);
                            Toast.makeText(getApplicationContext(),Note,Toast.LENGTH_LONG).show();
                        }
                    }

                    }


                }

        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                REGISTER_URL , "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject msg = response.getJSONObject("message");
                    Customer = msg.getString("Customer Ref");
                    Address=msg.getString("Address Disply");
                    Note=msg.getString("Delivery Note");
                    Trip=msg.getString("Trip");
                    shippingdate = msg.getString("Date");
                    emailid=msg.getString("Driver ID");
                    if(emailid.equals(UserID)){
                        Toast.makeText(Scan.this,"The product belongs to you",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(Scan.this,"The product doesn't belongs to you",Toast.LENGTH_LONG).show();
                    }
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                         date = formatter.parse(shippingdate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    DateFormat df = new SimpleDateFormat("\ndd\nMMM\n");
                    reportDate = df.format(date);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
//                Intent i=new Intent(Scan.this,DeliveryDetail.class);
//                i.putExtra("n",substr);
//                i.putExtra("a",Address);
//                i.putExtra("r",Customer);
//                i.putExtra("note",Note);
//                i.putExtra("trip",Trip);
//                i.putExtra("d",reportDate);
//                startActivity(i);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public static class ScanFragment extends Fragment {
        private String toast;

        public ScanFragment() {
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            displayToast();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_scan, container, false);
            Button scan = (Button) view.findViewById(R.id.scan_from_fragment);
            scan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    scanFromFragment();
                }
            });
            return view;
        }

        public void scanFromFragment() {
            IntentIntegrator.forSupportFragment(this).initiateScan();
        }

        private void displayToast() {
            if(getActivity() != null && toast != null) {
                Toast.makeText(getActivity(), toast, Toast.LENGTH_LONG).show();
                toast = null;
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if(result != null) {
                if(result.getContents() == null) {
                    toast = "Cancelled from fragment";
                } else {
                    //toast = "Scanned from fragment: " + result.getContents();
                    toast = data.getStringExtra("SCAN_RESULT");

                }

                // At this point we may or may not have a reference to the activity
                displayToast();
            }
        }
    }
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return  true;
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//
//        if(id==R.id.logout)
//        {
//            logout l = new logout(this);
//            l.signOut();
//
//            SharedPreferences sharedpreferences = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.clear();
//            editor.commit();
//
//        }
//        return super.onOptionsItemSelected(item);
//
//    }
}
