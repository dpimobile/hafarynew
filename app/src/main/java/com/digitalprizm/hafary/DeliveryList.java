package com.digitalprizm.hafary;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

public class DeliveryList extends BaseActivity {
    int p,falg;
    String pos;
    static final String TAG = "DeliveryDetails";
    ListView lv;
    ArrayList<HashMap<String, String>> contactList;
    ArrayList<Integer> list = new ArrayList<Integer>();
    ListAdapter adapter;
    SQLiteDatabase db;
    public static final int MY_PERMISSIONS_REQUEST_FINE = 0;
    BasicClientCookie c = null;
    String a[] = {"name", "lat", "lon", "customer"};
    String name1, address, shippingdate, customer_id, customer_name,customer_note,customer_trip,contact_no,mobile_no,currentdateString,reportDate;
    int TranNo = 0;
    String strPost, strPut;
    String UserID, pass;
    String DBName, DeliveryTableSchema, DeliveryTableName, DeviceID, mode;
    boolean postData = false;
    Date date;
    TextView txtaddress,txtdate;
    public static final String DATA_SAVED_BROADCAST = "net.simplifiedcoding.datasaved";
    private BroadcastReceiver broadcastReceiver;
    RelativeLayout rl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.main);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.actionbar);
            TextView title = (TextView) findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
            ImageView img=(ImageView)findViewById(getResources().getIdentifier("imageView", "id", getPackageName()));
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logout l = new logout(DeliveryList.this);
                    l.signOut();

                    SharedPreferences sharedpreferences = getSharedPreferences(Login.SysPrefs, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.clear();
                    editor.commit();
                }
            });
            title.setText("MY DELIVERY");
            txtaddress = (TextView) findViewById(R.id.address);
            bottomNavigationView.getMenu().getItem(1).setChecked(true);
            rl=(RelativeLayout)findViewById(R.id.rl);
            lv = (ListView) findViewById(R.id.listView);
            txtdate=(TextView)findViewById(R.id.date);
            currentdateString = new SimpleDateFormat("dd MMM yyyy").format(new Date());
            txtdate.setText(currentdateString);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //return true;
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_FINE);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }

            }
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View v,
                                        final int position, long id) {

                }
            });
            lv.setRecyclerListener(new AbsListView.RecyclerListener() {
                @Override
                public void onMovedToScrapHeap(View view) {
                    // Stop animation on this view
                }
            });
            singleClass.mHttpClient = new DefaultHttpClient();
            SharedPreferences myPrefs = this.getSharedPreferences(Login.MyPREFERENCES, MODE_PRIVATE);
            UserID = myPrefs.getString("usercode", "DEFAULT VALUE");
            pass = myPrefs.getString("password", "DEFAULT VALUE");
            if (UserID != "DEFAULT VALUE" || pass != "DEFAULT VALUE") {
                new DataAsync("").execute();
            }
            contactList = new ArrayList<>();
            DBName = getResources().getString(R.string.DBName);
            DeliveryTableName = getResources().getString(R.string.DeliveryTable);
            DeliveryTableSchema = getResources().getString(R.string.DeliveryTableSchema);
            db = openOrCreateDatabase(DBName, MODE_PRIVATE, null);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + DeliveryTableName + DeliveryTableSchema + ";");

            Cursor crs = db.rawQuery("Select MAX(TRAN_NO) AS TRAN_NO from " + DeliveryTableName, null);
            if (crs != null) {
                if (crs.getCount() > 0) {
                    if (crs.moveToFirst()) {
                        TranNo = crs.getInt(crs.getColumnIndex("TRAN_NO"));
                        TranNo++;
                    }
                }
            }
        } catch (
                Exception e
                )

        {

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
    }
    private StringRequest createRequest() {
        StringRequest myReq = new StringRequest(Request.Method.POST,
                "http://hafarydev.digitalprizm.net/api/method/login?usr=" + UserID + "&pwd=" + pass,
                createMyReqSuccessListener(),
                createMyReqErrorListener());
        return myReq;
    }

/*Get data from api*/

    private StringRequest createRequest1() {
        String url = "";

        try {
            String encodeUrl = URLEncoder.encode(UserID, "UTF-8");
            url = "http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.get_delivery_schedule_list?user_id=" + encodeUrl;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringRequest myReq = new StringRequest(Request.Method.GET,
                url,
                createMyReqSuccessListener(),
                createMyReqErrorListener());
        return myReq;
    }

    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        ProgressDialog pdLoading = new ProgressDialog(DeliveryList.this);
        final AlertDialog ad = new AlertDialog.Builder(getApplicationContext()).create();
        String DownloadWithError = null;
        public String OPERATION_NAME = "GetMasterDS";
        public final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
        public String SOAP_ADDRESS = "";
        private int Cntr = 0;
        public final String SOAP_ACTION = "http://tempuri.org/GetMasterDS";
        public String mstrMasterName, mstrFilter, xmlString;

        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (!isInternetWorking()) {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }
            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ad.setMessage("Done");
            if (DownloadWithError == "Please check internet connection") {

                //Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
                Snackbar snackbar=Snackbar.make(rl,"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        singleClass.mQueue.add(createRequest1());
                    }
                });
                snackbar.setActionTextColor(Color.RED);
                View sbview=snackbar.getView();
                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.YELLOW);
                snackbar.show();
            } else {
                singleClass.mQueue.add(createRequest1());
                //singleClass.mQueue.add(createPost());
            }
        }
    }

    private Response.Listener<String> createMyReqSuccessListener() {

        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CookieStore cs = singleClass.mHttpClient.getCookieStore();
                // BasicClientCookie c = (BasicClientCookie) getCookie(cs, "sid");
                c = (BasicClientCookie) getCookie(cs, "sid");
                cs.addCookie(c);
                String res = "";
                if (response != null && response.contains("message")) {
                    try {
                        JSONObject jsonObj = new JSONObject(response);
                        JSONArray contacts = jsonObj.getJSONArray("message");
                        for (int i = 0; i < contacts.length(); i++) {
                            JSONObject c1 = contacts.getJSONObject(i);
                            name1 = c1.getString("customer_ref");
                            address = c1.getString("Address");
                            customer_name = c1.getString("name");
                            customer_note = c1.getString("delivery_note_no");
                            customer_trip = c1.getString("trip");
                            shippingdate = c1.getString("date");
                            contact_no = c1.getString("contact_no");
                            mobile_no = c1.getString("mobile_no");
                            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                date = formatter.parse(shippingdate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            DateFormat df = new SimpleDateFormat("dd MMM yyy");
                            reportDate = df.format(date);
                            final HashMap<String, String> contact = new HashMap<>();
                            contact.put("customer_ref", name1);
                            contact.put("name", customer_name);
                            contact.put("note", customer_note);
                            contact.put("address_display", address);
                            contact.put("trip", customer_trip);
                            contact.put("date", reportDate);
                            contact.put("contact", contact_no);
                            contact.put("mobile", mobile_no);
                            contactList.add(contact);
                        }
//                        adapter = new SimpleAdapter(getApplicationContext(), contactList,
//                                R.layout.list_item_, new String[]{"note", "customer_ref", "trip", "date", "address_display"}
//                                , new int[]{R.id.textName, R.id.textAge,R.id.tripno, R.id.date, R.id.textdate}) {
//                        if (currentdateString == reportDate) {
                            adapter = new SimpleAdapter(getApplicationContext(), contactList,
                                    R.layout.list_item, new String[]{"customer_ref", "trip", "note", "address_display"}
                                    , new int[]{R.id.cust, R.id.trip, R.id.deliveryno, R.id.address}) {
                                Button btnstart, bphoto;
                                ImageView imgphone, imgmsg, imgnav;

                                @Override
                                public View getView(final int position, View convertView, ViewGroup parent) {
                                    View v = super.getView(position, convertView, parent);
                                    btnstart = (Button) v.findViewById(R.id.btnEdit);
                                    bphoto = (Button) v.findViewById(R.id.btnDelete);
                                    imgphone = (ImageView) v.findViewById(R.id.phone);
                                    imgmsg = (ImageView) v.findViewById(R.id.msg);
                                    imgnav = (ImageView) v.findViewById(R.id.nav);
//                                GradientDrawable gd = new GradientDrawable();
//                                gd.setColor(Color.parseColor("#63972D")); // Changes this drawbale to use a single color instead of a gradient
//                                gd.setCornerRadius(15);
                                    // btnstart.setBackgroundDrawable(gd);
                                    btnstart.setBackgroundColor(Color.parseColor("#91dc5b"));
                                    btnstart.setText("Start");
                                    btnstart.setTextColor(Color.WHITE);
                                    if (btnstart.getText().toString().equalsIgnoreCase("Stopped")) {
                                        btnstart.setText("Start");
                                        //falg=1;
                                        btnstart.setTextColor(Color.WHITE);
                                    } else {
                                        btnstart.setText("Start");
                                        btnstart.setTextColor(Color.WHITE);
                                    }
//                                    if (pos != null) {
//                                        btnstart.getTag(position);
//                                        if (position == p) {
//                                            btnstart.setBackgroundColor(Color.parseColor("#ef384e"));
////                                        gd.setColor(Color.parseColor("#D80027")); // Changes this drawbale to use a single color instead of a gradient
////                                        gd.setCornerRadius(15);
////                                        btnstart.setBackgroundDrawable(gd);
//                                            if (falg == 1) {
//                                                btnstart.setText("Stopped");
//                                                btnstart.setTextColor(Color.WHITE);
//                                                // falg=0;
//                                            } else {
//                                                btnstart.setText("Stop");
//                                                btnstart.setTextColor(Color.WHITE);
//                                            }
//
//                                        }

                                    if (pos != null) {
                                        btnstart.getTag(position);
                                        if (list.contains(position)) {
                                            btnstart.setBackgroundColor(Color.parseColor("#ef384e"));

                                            if (falg == 1) {
                                                btnstart.setText("Stop");
                                                btnstart.setTextColor(Color.WHITE);
                                                btnstart.setBackgroundColor(Color.parseColor("#ef384e"));                         // falg=0;
                                            }else {
                                                btnstart.setText("Stop");
                                                btnstart.setTextColor(Color.WHITE);
                                                btnstart.setBackgroundColor(Color.parseColor("#ef384e"));
                                            }

                                        }
                                    }
                                    btnstart.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            HashMap<String, Object> obj = (HashMap<String, Object>) adapter.getItem(position);
                                            String name = (String) obj.get("name");

                                            // list.add(p);
                                            Button btnstartstop = (Button) view;
                                            btnstartstop.setTag(position);
                                            //  if(mHighlightedPositions[position]) {
                                            if (btnstartstop.getText().toString().equalsIgnoreCase("Start")) {
//
//                                                GradientDrawable gd = new GradientDrawable();
//                                                gd.setColor(Color.parseColor("#D80027")); // Changes this drawbale to use a single color instead of a gradient
//                                                gd.setCornerRadius(15);
//                                                btnstartstop.setBackgroundDrawable(gd);
//                                                btnstartstop.setText("Stop");
//                                                btnstartstop.setTextColor(Color.WHITE);

                                                //notifyDataSetChanged();
                                                if (isServiceRunning(MyServices.class)) {
                                                    Toast.makeText(getApplicationContext(), "Service is already running...", Toast.LENGTH_SHORT).show();
                                                    // Stop the service

                                                } else {
                                                    View parentRow = (View) view.getParent();
                                                    ListView lv = (ListView) parentRow.getParent();
                                                    p = lv.getPositionForView(parentRow);
                                                    pos = String.valueOf(p);
                                                    list.add(p);
                                                    btnstartstop.setBackgroundColor(Color.parseColor("#ef384e"));
                                                    btnstartstop.setText("Stop");
                                                    btnstartstop.setTextColor(Color.WHITE);
                                                    Intent i = new Intent(getApplicationContext(), MyServices.class);
                                                    i.putExtra("n", name);
                                                    getApplicationContext().startService(i);
                                                    falg = 0;
                                                }
                                            } else {
//                                                View parentRow = (View) view.getParent();
//                                                ListView lv = (ListView) parentRow.getParent();
//                                                p = lv.getPositionForView(parentRow);
//                                                pos = String.valueOf(p);
//                                                list.add(p);
                                                btnstartstop.setText("Stop");

                                                falg = 1;
                                                Intent i = new Intent(getApplicationContext(), MyServices.class);
                                                getApplicationContext().stopService(i);
                                             Toast.makeText(getApplicationContext(),"The service is stopped",Toast.LENGTH_LONG).show();
                                            }

                                        }
                                    });

                                    bphoto.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            HashMap<String, Object> obj = (HashMap<String, Object>) adapter.getItem(position);
                                            String name = (String) obj.get("name");
                                            String custref = (String) obj.get("customer_ref");
                                            String shipdate = (String) obj.get("date");
                                            String custaddress = (String) obj.get("address_display");
                                            String customernote = (String) obj.get("note");
                                            String customertrip = (String) obj.get("trip");
                                            String contactno = (String) obj.get("contact");
                                            String mobileno = (String) obj.get("mobile");
                                            Intent i = new Intent(DeliveryList.this, DeliveryDetail.class);
                                            i.putExtra("n", name);
                                            i.putExtra("r", custref);
                                            i.putExtra("a", custaddress);
                                            i.putExtra("d", shipdate);
                                            i.putExtra("note", customernote);
                                            i.putExtra("trip", customertrip);
                                            i.putExtra("contact", contactno);
                                            i.putExtra("mobile", mobileno);
                                            startActivityForResult(i, 1);

                                        }
                                    });
                                    imgphone.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            HashMap<String, Object> obj = (HashMap<String, Object>) adapter.getItem(position);
                                            String contactno = (String) obj.get("contact");
                                            String mobileno = (String) obj.get("mobile");
                                            Intent i = new Intent(DeliveryList.this, Call.class);
                                            String custref = (String) obj.get("customer_ref");
                                            i.putExtra("r", custref);
                                            i.putExtra("contact", contactno);
                                            i.putExtra("mobile", mobileno);
                                            startActivityForResult(i, 1);
                                        }
                                    });
                                    imgmsg.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            HashMap<String, Object> obj = (HashMap<String, Object>) adapter.getItem(position);
                                            String mobileno = (String) obj.get("mobile");
                                            String custref = (String) obj.get("customer_ref");
                                            Intent i = new Intent(DeliveryList.this, SmsActivity.class);
                                            i.putExtra("r", custref);
                                            i.putExtra("mobile", mobileno);
                                            startActivityForResult(i, 1);
                                        }
                                    });
                                    imgnav.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            HashMap<String, Object> obj = (HashMap<String, Object>) adapter.getItem(position);
                                            String custaddress = (String) obj.get("address_display");
                                            Intent intent = new Intent();
                                            intent.setAction(Intent.ACTION_VIEW);
                                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                            intent.setData(Uri.parse("http://maps.google.com/maps?daddr=" + custaddress));
                                            startActivityForResult(intent, 1);
                                        }
                                    });
                                    return v;
                                }

                                private boolean isServiceRunning(Class<?> serviceClass) {
                                    ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

                                    // Loop through the running services
                                    for (ActivityManager.RunningServiceInfo service : activityManager.getRunningServices(Integer.MAX_VALUE)) {
                                        if (serviceClass.getName().equals(service.service.getClassName())) {
                                            // If the service is running then return true
                                            return true;
                                        }
                                    }
                                    return false;
                                }
                            };

                            lv.setItemsCanFocus(false);
                        Parcelable state = lv.onSaveInstanceState();
                        lv.onRestoreInstanceState(state);
                            lv.setAdapter(adapter);

                        lv.setTextFilterEnabled(true);

//                        }else{Toast.makeText(DeliveryList.this,"No delivery today",Toast.LENGTH_LONG).show();}
                    }catch( final JSONException e){

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(),
                                            "Some error ocurred. Try again !!",
                                            LENGTH_LONG)
                                            .show();
                                }
                            });

                        }
                    }

                else {

                   singleClass.mQueue.add(createRequest1());

                }

            }
        };

    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
    }

    public Cookie getCookie(CookieStore cs, String cookieName) {
        Cookie ret = null;
        List<Cookie> l = cs.getCookies();
        for (Cookie c : l) {
            if (c.getName().equals(cookieName)) {
                ret = c;
                break;
            }
        }
        return ret;
    }
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return  true;
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//
//        if(id==R.id.logout)
//        {
//            logout l = new logout(this);
//            l.signOut();
//            SharedPreferences sharedpreferences = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.clear();
//            editor.commit();
//
//        }
//        return super.onOptionsItemSelected(item);
//
//    }
}
