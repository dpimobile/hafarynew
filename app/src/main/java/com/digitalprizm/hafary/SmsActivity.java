package com.digitalprizm.hafary;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;

import android.util.Log;
import android.view.Menu;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import static android.widget.Toast.LENGTH_LONG;

public class SmsActivity extends BaseActivity {
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;
    Button sendBtn;
    TextView txtphoneNo;
    EditText txtMessage;
    String phoneNo,urlJsonObj;
    String message,Mobile,Customer_name;
    TextView Cust_Name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        TextView title = (TextView) findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        ImageView img=(ImageView)findViewById(getResources().getIdentifier("imageView", "id", getPackageName()));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout l = new logout(SmsActivity.this);
                l.signOut();

                SharedPreferences sharedpreferences = getSharedPreferences(Login.SysPrefs, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
            }
        });
        title.setText("SMS");
        bottomNavigationView.getMenu().getItem(1).setChecked(true);
        //SharedPreferences myPrefs = this.getSharedPreferences(DeliveryDetail.MyPREFERENCES, MODE_PRIVATE);
        //Customer_name=myPrefs.getString("cust","DEFAULT VALUE");
        Cust_Name=(TextView)findViewById(R.id.cust_name);
        sendBtn = (Button) findViewById(R.id.btnSendSMS);
        txtphoneNo = (TextView) findViewById(R.id.etEmail);
        txtMessage = (EditText) findViewById(R.id.etPwd);
        Intent intent = getIntent();
        Mobile=intent.getStringExtra("mobile");
        Customer_name=intent.getStringExtra("r");
        txtphoneNo.setText(Mobile);
        Cust_Name.setText(Customer_name);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return true;
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }

        }
        sendBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(txtMessage.getText().toString().equals(""))
                {
                    //Toast.makeText(getApplicationContext(),"Email cannot be blank", LENGTH_LONG).show();
                    txtMessage.setError("Email Id required !");
                }else{
                    String encodno=null;String encodmsg=null;
                    String msg=txtMessage.getText().toString();
                    try {

                        encodno = URLEncoder.encode(Mobile, "UTF-8");
                        encodmsg = URLEncoder.encode(msg, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    urlJsonObj="http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.send_message_api?mobile_no="+ encodno + "&message=" + encodmsg;
                    new DataAsync("").execute();
                }//sendSMSMessage();
            }
        });
    }
    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                urlJsonObj , "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
               // Log.d(TAG, response.toString());
                Toast.makeText(getApplicationContext(),
                        "SMS Sent", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),
                        "Something went wrong. Try again !!", Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        String DownloadWithError= null;
        public String SOAP_ADDRESS = "";
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }

            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(DownloadWithError == "Please check internet connection")
            {
                Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
//               // Snackbar snackbar=Snackbar.make(getView(),"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new DataAsync("").execute();
//                    }
//                });
//                snackbar.setActionTextColor(Color.RED);
//                View sbview=snackbar.getView();
//                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
//                tv.setTextColor(Color.YELLOW);
//                snackbar.show();

            }
            else
            {
                makeJsonObjectRequest();

            }

        }
    }
    protected void sendSMSMessage() {
        phoneNo = txtphoneNo.getText().toString();
        message = txtMessage.getText().toString();
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.",
                    Toast.LENGTH_LONG).show();
        }catch(Exception e){};
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }
}
