package com.digitalprizm.hafary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

//import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by dell-pc on 11-01-2017.
 */

public class logout {

    Context myContext;

    //auth = FirebaseAuth.getInstance();
    public logout(Context context){
        this.myContext = context;
    }

   /* void callMethod(){
        if(isFileValid){

        }else{
            ((Activity)myContext).finish();
        }
    }*/

    public  void signOut() {
        //auth = FirebaseAuth.getInstance();
        //auth.signOut();
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(myContext);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        Toast.makeText(myContext, "Logged out succcessfully", Toast.LENGTH_LONG).show();
        ((Activity)myContext).finish();
        Intent i = new Intent(myContext, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        myContext.startActivity(i);


    }


}
