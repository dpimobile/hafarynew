package com.digitalprizm.hafary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by Dell on 14-Jun-17.
 */

public class GPSDatabase {
    private Context context;
    private DbHelper dbHelper;
    public final String DBNAME="gps1";
    public final int DBVERSION=5;
    public SQLiteDatabase db;
    public final String COLUMN2="latitude";
    public final String COLUMN3="longitude";
    public final String COLUMN4="name";
    public final String COLUMN1="locationId";
    public final String COLUMN5="status";
    public final String TABLENAME="location";
    public final String TABLEIMG="image";
    public final String COLUMN6="imageid";
    public final String COLUMN7="image1";
    public final String COLUMN8="image2";
    public final String COLUMN9="image3";
    public final String COLUMN10="image4";
    public final String COLUMN11="nme";
    public final String CREATERDBIMG="create table image(imageid integer primary key autoincrement, nme text not null, image1 blob, image2 text, image3 text, image4 text);";
    public final String CREATERDB="create table location(locationId integer primary key autoincrement,latitude text not null, longitude text not null, name text not null, status text not null);";
    //const
    public GPSDatabase(Context context){
        this.context=context;
        dbHelper=new DbHelper(context);
    }
    public class DbHelper extends SQLiteOpenHelper {
        public DbHelper(Context context){
            super(context,DBNAME,null,DBVERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            db.execSQL(CREATERDB);
            db.execSQL(CREATERDBIMG);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }
    }
    public long insertRows(String column2, String column3, String column4, String column5){
        ContentValues value=new ContentValues();
        value.put(COLUMN2, column2);
        value.put(COLUMN3, column3);
        value.put(COLUMN4, column4);
        value.put(COLUMN5, column5);
        return db.insert(TABLENAME,null,value);
    }
    public long insertimgRow(String column11,byte[] column7, String column8, String column9, String column10){
        ContentValues value=new ContentValues();
        value.put(COLUMN11, column11);
        value.put(COLUMN7, column7);
        value.put(COLUMN8, column8);
        value.put(COLUMN9, column9);
        value.put(COLUMN10, column10);
        return db.insert(TABLEIMG,null,value);
    }
    public boolean deleteRow(long rowId) {
        return db.delete(TABLENAME, COLUMN1 + "=" + rowId, null) > 0;
    }
    public Cursor getAllRows(){
        Cursor cursor=db.query(TABLENAME, new String[]{COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5}, null,null, null, null, null);
        return cursor;
    }
    public Cursor getAllimgRows(){
        Cursor cursor=db.query(TABLEIMG, new String[]{COLUMN6,COLUMN11,COLUMN7,COLUMN8,COLUMN9,COLUMN10}, null,null, null, null, null);
        return cursor;
    }
    public boolean deleteimgRow(long rowId) {
        return db.delete(TABLEIMG, COLUMN6 + "=" + rowId, null) > 0;
    }
    public void open() throws SQLException {
        db=        dbHelper.getWritableDatabase();
        //return true;
    }
    public void close(){
        dbHelper.close();
        //return true;
    }
}
