package com.digitalprizm.hafary;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.widget.Toast.LENGTH_LONG;

public class AboutActivity extends BaseActivity {
    private String jsonResponse, urlJsonObj;
    private TextView txttile,txtdescription;
    RelativeLayout rl;
    private static String TAG = AboutActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);
        getSupportActionBar().setIcon(R.drawable.hafary);
        TextView title=(TextView)findViewById(getResources().getIdentifier("action_bar_title", "id", getPackageName()));
        ImageView img=(ImageView)findViewById(getResources().getIdentifier("imageView", "id", getPackageName()));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout l = new logout(AboutActivity.this);
                l.signOut();

                SharedPreferences sharedpreferences = getSharedPreferences(Login.SysPrefs, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
            }
        });
        title.setText("ABOUT US");

        bottomNavigationView.getMenu().getItem(3).setChecked(true);
        txttile=(TextView)findViewById(R.id.title);
        txtdescription=(TextView)findViewById(R.id.des);
        urlJsonObj="http://hafarydev.digitalprizm.net/api/method/delivery_management.api.api.get_about";
        new DataAsync("").execute();
    }
    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj , "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object

                    //JSONObject jsonObj = new JSONObject(response);
                    JSONObject msg = response.getJSONObject("message");
                   // JSONArray msg = response.getJSONArray("message");
                   // for (int i = 0; i < msg.length(); i++) {
                     //   JSONObject c1 = msg.getJSONObject(i);
                        String field = msg.getString("description");
                        String value = msg.getString("title");

                        jsonResponse = "";
                        jsonResponse += "" + field + "\n\n";
                       // jsonResponse += "title" + value + "\n\n";
                    String htmlTextStr = Html.fromHtml(jsonResponse).toString();
                    String htmltextstr = Html.fromHtml(value).toString();
                    txttile.setText(htmltextstr);
                        txtdescription.setText(htmlTextStr);
//                        Toast.makeText(getApplicationContext(),
//                                "hi",
//                                LENGTH_LONG).show();
                    //}
//                    if (imageurl == "null") {
//                        img.setBackgroundResource(R.drawable.user_);
//                    } else  {
//                        Picasso.with(Driver_Det.this).load(image).into(img);
//                    }

                    // if (msg.has("Profile Picture"))

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Something went wrong. Try again !!",
                            LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Error", Toast.LENGTH_SHORT).show();
                // hide the progress dialog

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
    private class DataAsync extends AsyncTask<String, String, String> {
        private String resp;
        String DownloadWithError= null;
        public String SOAP_ADDRESS = "";
        public DataAsync(String Url) {
            super();
            SOAP_ADDRESS = Url;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            try {

                if(!isInternetWorking())
                {
                    DownloadWithError = "Please check internet connection";
                }

            } catch (Exception a) {
                a.printStackTrace();
                DownloadWithError = a.getMessage();
            }

            return DownloadWithError;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(DownloadWithError == "Please check internet connection")
            {
                Toast.makeText(getApplicationContext(), "Please check if internet is working", LENGTH_LONG).show();
//                Snackbar snackbar=Snackbar.make(rl,"Check internet connection",Snackbar.LENGTH_LONG).setAction("RETRY", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new DataAsync("").execute();
//                    }
//                });
//                snackbar.setActionTextColor(Color.RED);
//                View sbview=snackbar.getView();
//                TextView tv=(TextView)sbview.findViewById(android.support.design.R.id.snackbar_text);
//                tv.setTextColor(Color.YELLOW);
//                snackbar.show();

            }
            else
            {
                makeJsonObjectRequest();

            }

        }
    }
}
